<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Surely WorldWide</title>
  <meta name="Worldwide implementation of Indeed Job Search API, transcending borders and languages" content="">
  <meta name="MJLite" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/grave.css">

  <!-- JS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <script type="text/javascript" src="js/fc.js"></script>
  <script type="text/javascript" src="http://gdc.indeed.com/ads/apiresults.js"></script>


  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <div class="container">
    <!-- Navbar -->
    <div class="container">
      <div class="row">
        <div class="one column" style="margin-top: 0%; margin-right: 4%">
          <p>Surely</p>
        </div>
        <div class="one column" style="margin-top: 0%; margin-right: 4%">
          <p>Search</p>
        </div>
        <div class="one column" style="margin-top: 0%; margin-right: 4%">
          <p>Lang</p>
        </div>
        <div class="one column" style="margin-top: 0%; margin-right: 4%">
          <p>Country</p>
        </div>
        <div class="three columns" style="margin-top: 0%">
          <p></p>
        </div>
        <div class="one column" style="margin-top: 0%; margin-right: 4%">
          <p>Official</p>
        </div>
        <div class="one column" style="margin-top: 0%; margin-right: 4%">
          <p>About</p>
        </div>
      </div>
    </div>

    <!-- Search controls -->
    <div class="container">
      <div class="row">
        <div class="four columns" style="margin-top: 20%">
          <h4>What</h4>
          <input class="u-full-width" type="search" placeholder="job title, keywords, or company" id="jobQuery">
        </div>
        <div class="four columns" style="margin-top: 20%">
          <h4>Where</h4>
          <input class="u-full-width" type="search" placeholder="city, province, or 'remote'" id="locationQuery">
        </div>
        <div class="four columns" style="margin-top: 20%">
          <h4>Find it</h4>
          <input class="button-primary" type="submit" value="Search" onclick="generateURL()">
        </div>
      </div>
    </div>
  </div>

  <!-- Results -->
  <div class="container" style="margin-top: 5%">
    <div id="res-table">
      <table class="u-full-width">
        <thead>
          <tr>
            <th>Job</th>
            <th>Location</th>
            <th>Post Date</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Engineer</td>
            <td>Helsinki</td>
            <td>Today</td>
            <td>Engineering stuff</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <?php
    echo "Testing php script";
  ?>

  <!-- Footer -->
  <div class="container" style="margin-top: 20%">
    <div class="footer">
      <p>C 2020 Surely
      <span id="indeed_at"><a title="Job Search" href="https://www.indeed.com" rel="nofollow" >jobs by <img alt=Indeed src="https://www.indeed.com/p/jobsearch.gif" style="border: 0; vertical-align: middle;"></a></span>
      </p>
    </div>
  </div>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
