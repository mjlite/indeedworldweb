var resultsTable;

function hideResults() {
  resultsTable = document.getElementById("res-table");
  if (resultsTable.style.display === "none") {
    resultsTable.style.display = "block";
  } else {
    resultsTable.style.display = "none";
  }
}

if (window.addEventListener)
  window.addEventListener("load", hideResults, false);
else
  window.onload = hideResults;

function generateURL() {
    var job = document.getElementById("jobQuery");
    var location = document.getElementById("locationQuery");
    var publisherID = "444457643170882";
    var version = 2;
    var format = "xml";
    var radius = document.getElementById("jobRadius");
    var type = document.getElementById("jobType");
    var limit = document.getElementById("jobLimit");
    var fromAge = document.getElementById("jobAge");
    var highlight = 1;
    var filter = 1;
    var country = document.getElementById("jobCountry");
    var userIP = "0.0.0.0";
    var userAgent = "";

    console.log("Job query: "+job.value);
    console.log("Location query: "+location.value);
    resultsTable.style.display = "block";
}

